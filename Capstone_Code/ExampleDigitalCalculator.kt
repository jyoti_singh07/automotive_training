package com.example.capstone2
import org.junit.Test
import org.junit.Assert.assertEquals
import org.junit.Before
class ExampleDigitalCalculator {
    private lateinit var calculator : Calculator
    @Before
    fun setUp() {
        calculator = Calculator()
    }
    @Test
    fun testAddition() {
        calculator.setInput1(5)
        calculator.setInput2(3)
        assertEquals(8, calculator.add())
    }
    @Test
    fun testSubtraction() {
        calculator.setInput1(5)
        calculator.setInput2(3)
        assertEquals(2, calculator.subtract())
    }
    @Test
    fun testMultiplication() {
        calculator.setInput1(5)
        calculator.setInput2(3)
        assertEquals(15, calculator.multiply())
    }
    @Test
    fun testDivision() {
        calculator.setInput1(6)
        calculator.setInput2(3)
        assertEquals(2, calculator.divide())
    }

}