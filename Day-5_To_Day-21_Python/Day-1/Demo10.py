'''Write a Python program to input marks of five subjects Physics, Chemistry,
Biology, Mathematics and Computer. Calculate percentage and grade according to
following:
Percentage >= 90% : Grade A
Percentage >= 80% : Grade B
Percentage >= 70% : Grade C
Percentage >= 60% : Grade D
Percentage >= 40% : Grade E
Percentage < 40% : Grade F
'''

phy=float(input("Enter marks obtained in physics= "))
chem=float(input("Enter marks obtained in chemistry= "))
bio=float(input("Enter marks obtained in biology= "))
maths=float(input("Enter marks obtained in mathematics= "))
com=float(input("Enter marks obtained in computer= "))
per=(phy +chem+bio+maths+com)/500*(100)
if(per >=90):
    print("Grade A")
elif(per>=80):
    print("Grade B")
elif(per>=70):
    print("Grade C")
elif(per>=60):
    print("Grade D")
elif(per>=40):
    print("Grade E")
elif(per<40):
    print("Grade F")

