'''
Write a python program to input electricity unit charges and calculate total
electricity bill according to the given condition:
For first 50 units Rs. 0.50/unit
For next 100 units Rs. 0.75/unit
For next 100 units Rs. 1.20/unit
For unit above 250 Rs. 1.50/unit
An additional surcharge of 20% is added to the bill
'''
unit=float(input("Enter total unit= "))
if(unit>0):
    if(unit<=50):
        total_bill1=unit*(0.50)+(unit/100)*20
        print(total_bill1)
    elif(unit>50 and unit<=150):
        total_bill2=(unit*(0.75)+(unit/100)*20)
        print(total_bill2)
    elif(unit>150 and unit<=250):
        total_bill3=(unit*(1.20)+(unit/100)*20)
        print(total_bill3)
    elif(unit>250):
        total_bill4=(unit*(1.50)+(unit/100)*20)
        print(total_bill4)

