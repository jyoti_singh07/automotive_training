def func1():
    print("Inside Func1")
    return 10

def func2():
    print("Inside Func2")
    num=func1()
    return num

def func3():
    print("Inside Func3")
    num=func2()
    num=num*5
    return num

val=func3()
print(val)

def verify(num1,num2):
    if num1 > num2:
        return num1
    elif num1 == num2:
        return 1
    else:
        return num2

def display(arg1,arg2):
    if(verify(arg1,arg2)==arg1):
        print("A")
    elif(verify(arg1,arg2)==1):
        print("C")
    else:
        print("B")

display(1000,3500)


def check_value(message,num):
    msg=message[:num]
    return len(msg)
result=check_value('Infosys',len('Infosys'))
print("Result:", result)

