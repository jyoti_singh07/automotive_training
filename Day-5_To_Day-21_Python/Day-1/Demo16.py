def calculate_total_ticket_cost(no_of_adults, no_of_children):
     total_ticket_cost=0
     servicetax=0.07
     discount=0.10
     rateperadult=37550
     rateperchild=(1/3)*37550
     costperadult=rateperadult*no_of_adults
     costperchild=rateperchild*no_of_children
     totalcost=(costperadult+costperchild)*servicetax+(costperadult+costperchild)
     total_ticket_cost=totalcost-totalcost*discount
 
     return total_ticket_cost

print(calculate_total_ticket_cost(4,2))

