'''checks if a number is a Buzz number or not. A number is a Buzz number if it
ends with 7 or is divisible by 7. 63 is a Buzz number as it is divisible by 7.
747 is also a Buzz number as it ends with 7. 83 is not a Buzz number as it is
neither divisible by 7 nor it ends in 7.
has context menu
'''
num=int(input("Enter a number= "))
if(num %7==0 or num %10==7):
    print("Number is a buzz number")
else:
    print("Number is not a buzz number")
