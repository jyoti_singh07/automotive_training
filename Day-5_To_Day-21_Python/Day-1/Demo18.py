'''You have x no. of 5 rupee coins and y no. of 1 rupee coins.
You want to purchase an item for amount z. The shopkeeper wants you to
provide exact change. You want to pay using minimum number of coins.
How many 5 rupee coins and 1 rupee coins will you use? If exact change is not
possible then display -1.

Sample Input                                                             Expected Output
available rs 1 coins   available rs 5 notes   amount to be made           rs 1 coind needed    rs 5 notes needed
      2                       4                    21                         1                  4
      11                      2                    11                          1                    2
      3                       3                     19                                -1
'''

def coin_exchange(amount_to_be,no_of_one_rupee_coin,no_of_five_rupee_coin):
    amt_of_one_rupee=1*no_of_one_rupee_coin
    amt_of_five_rupee=5*no_of_five_rupee_coin
    total_amt_avail=amt_of_one_rupee + amt_of_five_rupee
    if amount_to_be < total_amt_avail:
        return -1
    else:
        no_of_five_rupee_coin=amount_to_be//5
        no_of_one_rupee_coin=amount_to_be%5
        print("No of 5 rupee coin",no_of_five_rupee_coin)
        print("No of 1  rupee coin",no_of_one_rupee_coin)
print(coin_exchange(21,2,4))
print(coin_exchange(11,1,2))

