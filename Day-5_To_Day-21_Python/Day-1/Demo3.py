"""
Problem Statement
Write a python function to check whether three given numbers can form the sides of a triangle. 
Hint: Three numbers can be the sides of a triangle if none of the numbers are greater than or equal to the sum of the other two numbers.
"""
side1=int(input("Enter side1 of the triangle= "))
side2=int(input("Enter side2 of the triangle= "))
side3=int(input("Enter side3 of the triangle= "))
if((side1<(side2 +side3)) and (side2<(side1+side3)) and (side3<(side1+side2))):
    print("Triangle can be formed")
else:
    print("Triangle cannot be formed")

