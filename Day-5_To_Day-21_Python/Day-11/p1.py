"""
Given a word W, write a java program to perform the following. 
If the length of the word is greater than or equal to 3 then check the
following 
If the 2nd char is an Alphabet and a vowel display “vowel” 
If the 2nd char is an Alphabet and not a vowel display “consonant” 
If the 2nd char is a digit display “digit” 
If any other char display “other” 
if the length of the word is less than 3, display “invalid” 
 
Constraints:  
W is not null 
  
Input Format:  
First line of the input contains W 
 
Output Format:  
Generated output in a single line. 
Sample Input & Output  
Input:  Output  
 H2o    digit
 """
        


import re
W="H2o"
def check_word(W):
    if len(W) < 3:
        print("invalid")
        return
    
    second_char = W[1]
    
    if re.match(r'[a-zA-Z]', second_char):
        if second_char.lower() in ['a', 'e', 'i', 'o', 'u']:
            print("vowel")
        else:
            print("consonant")
    elif re.match(r'\d', second_char):
        print("digit")
    else:
        print("other")

# Reading input from standard input (stdin)
#W = input().strip()
print(check_word(W))



import re
word="H2o"
alphabets=re.findall(r"[a-zA-Z0-9]", word)
vowels=re.findall(r"[aeiouAEIOU]", word)
digits=re.findall(r"[0-9]", word)
 
 
if len(word)>=3:
    if word[1] in alphabets and word[1] in vowels:
        print("vowel")
    elif word[1] in alphabets and word[1] not in vowels and word[1] not in digits:
        print("consonant")
    elif word[1] in alphabets and word[1] in digits and word[1] not in vowels:
        print("digit")
    else:
        print("other")
else:
    print("invalid")

