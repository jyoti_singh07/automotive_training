"""
Write a Python program that matches a string that has an 'a' followed by
anything, ending in 'b'.

# Your code here
"""
import re
string="tanyadggbbb"
pattern=re.compile(r"[a].[b$]")
matches=re.findall(pattern,string)
print(matches)
