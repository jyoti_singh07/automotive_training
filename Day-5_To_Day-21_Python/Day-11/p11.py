""" 10) Write a Python program that matches a word at the beginning of a string
"""
import re
text="bnag abhha abba"
pattern=re.compile(r"\w+")
matches=bool(re.match(pattern,text))
print(matches)
