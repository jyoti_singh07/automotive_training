"""
11) Write a Python program that matches a word at the end of string, with optional
punctuation.
"""
import re
text="babaa'aabb!"
pattern=re.compile(r"\w+[.|!|?|']$")
match=bool(re.findall(pattern,text))
print(match)
