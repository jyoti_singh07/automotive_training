"""
15) Write a Python program where a string will start with a specific number.
"""

import re
string="255python"
pattern=re.compile(r"^\d+\w+$")
match=bool(re.match(pattern,string))
print(match)
