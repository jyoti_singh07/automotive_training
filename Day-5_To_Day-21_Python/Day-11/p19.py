"""
18) Write a Python program to search the numbers (0-9) of length between 1 to 3 in
a given string.

# Your code here
"""

import re
string="She is the best.She is the only employee in her family.She earns 30000 in a month"
pattern=re.compile(r"[0-9]{1,3}")
matches=re.findall(pattern,string)
for match in matches:
    print(match)
