"""
1) Write a Python program to check that a string contains only a certain set
of characters (in this case a-z, A-Z and 0-9).
"""

import re
text="Hello Good morning Helpline number is 123"
pattern=re.compile(r"[a-zA-Z0-9]+")
matches=re.finditer(pattern,text)
print(matches)
if matches:
    print("Found")
else:
    print("Not Found")
for match in matches:
    print(match.group())
    
