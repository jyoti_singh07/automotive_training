"""
19) Write a Python program to search some literals strings in a string.
Go to the editor Sample text : 'The quick brown fox jumps over the lazy dog.'
 Searched words : 'fox', 'dog', 'horse'
 """
import re
string='The quick brown fox jumps over the lazy dog.'
pattern="fox|dog|horse"
matches=re.finditer(pattern,string)
for match in matches:
    if match:
        print("Matched",match.start(),match.end(),match.group())
    else:
        print("Not matched",match.start(),match.end(),match.group())
