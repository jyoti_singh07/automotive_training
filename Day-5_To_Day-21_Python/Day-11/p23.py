""""
22) Write a Python program to find the occurrence and position of the substrings within a string.
text = 'Python exercises, PHP exercises, C# exercises'
pattern = 'exercises'
"""
import re
text = 'Python exercises, PHP exercises, C# exercises'
pattern="e"
matches=re.finditer(pattern,text)
occurrence=0
for match in matches:
    occurrence +=1
    if match:
        print("matched at",match.start(),match.end())
    else:
        print("not matched")
print(occurrence)  
