"""
23) Write a Python program to replace whitespaces with an underscore and
vice versa.
"""
import re
string="hi how are_you"
patterns=[" ", "_"]
final_string=""
for pattern in patterns:
    if pattern ==" ":
        final_string="".join(re.sub(pattern,"_",string))
   # else:
       # final_string="".join(re.sub(pattern," ",string))
print(final_string)


import re
string="The quick brown fox_jumps over the lazy dog"
patterns=["\\s","_","\\+"]
for pattern in patterns:
    if pattern =="\\s":
        string=re.sub(pattern,"_",string)
    elif pattern=="_":
        string=re.sub(pattern," ",string)
    else:
        string=re.sub(pattern,"_",string)
print(string)
        
