"""
25) Write a Python program to convert a date of yyyy-mm-dd format to dd-mm-yyyy
format.
"""
import re
date="2024-07-05"
pattern=re.compile(r'(\d{4})-(\d{1,2})-(\d{1,2}),\\3-\\2-\\1')
match=re.sub(pattern,date)
print(match)
