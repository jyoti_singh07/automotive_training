"""
26)Write a Python program to match if two words from a list of words starting with letter 'P'.
"""
import re
lst=["Apple","Pencil","Parrot"]
pattern="[A-Za-z]+[P]$"
matches=re.findall(pattern,word)
if len(matches)>=2:
    print("Two words starting from 'P' found")
else:
    print("Two words starting from 'P' not found")

