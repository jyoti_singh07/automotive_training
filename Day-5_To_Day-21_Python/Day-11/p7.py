"""
Write a Python program that matches a string that has an a followed by
 two to three 'b'.

# Your code here
"""
import re
string="hjjbbbbb"
pattern=re.compile(r"[a][b]{3}")
matches=re.findall(pattern,string)
print(matches)
