""" Write a Python program to find sequences of lowercase
 letters joined with a underscore.
"""
# Your code here
import re
text="habagtbb_aababa"
pattern=re.compile(r"[a-z]+_[a-z]+")
matches=re.findall(pattern,text)
print(matches)
