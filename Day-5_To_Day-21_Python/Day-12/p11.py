class OnlinePortal:
    item_list=["Redmi","Oppo","Sony","Poco"]
    quantity_list=[6,10,9,7]
    price_list=[12000,15000,22000,11000]
    @classmethod
    def search_item(cls,item):
        if item in cls.item_list:
            return cls.item_list.index(item)
        else:
            return -1
    @classmethod
    def place_order(cls,index,emi,quantity):
        cls.quantity_list[index]-= quantity
        total_price=quantity * cls.price_list[index]
        final_price=total_price
        print(total_price)
        if emi>0:
            return total_price+total_price *.02
            
        else:
            emi=1
            total_price - total_price *.02
        
    @classmethod
    def add_stock(cls,item_name,quantity):
        for item  in cls.item_list:
            if item ==item_name:
                index_of_item=cls.item_list.index(item)
                if cls.quantity_list[index_of_item]<=10:
                    cls.quantity_list[index_of_item]+=quantity
                    return "Stock Added Successfully"
                else:
                    return -1
    @classmethod
    def add_item(cls,item_name,price,quantity):
        if item_name not in cls.item_list:
            cls.item_list.append(item_name)
            cls.quantity_list.append(quantity)
            cls.price_list.append(price)
            #return "Item Added Successfully"
        else:
            return "Item Added Successfully"
            

class Buyer:
    def __init__(self,name,email_id):
        self.name=name
        self.email_id=email_id

    def purchase(self,item_name,quantity,emi):
        self.item_name=item_name
        self.quantity=quantity
        self.emi=emi
        if self.item_name in OnlinePortal.item_list:
            index_of_item=OnlinePortal.item_list.index(self.item_name)
            if OnlinePortal.quantity_list[index_of_item]>=self.quantity:
                return OnlinePortal.place_order(index_of_item,self.emi,self.quantity)
            else:
                return -1
        return -2

buyer1=Buyer("Jyoti","jyoti@gmail.com")
print(buyer1.purchase("Oppo",2,3))
print(OnlinePortal.search_item("Poco"))
print(OnlinePortal.add_stock("Poco",5))
print(OnlinePortal.add_item("Redmi",17000,30))
print(OnlinePortal.item_list)
print(OnlinePortal.quantity_list)
print(OnlinePortal.price_list)


    
   
        
