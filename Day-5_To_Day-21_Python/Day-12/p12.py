import functools as func
class Product:
    def __init__(self,id,name,price):
        self.id=id
        self.name=name
        self.price=price
    def set_id(self,id):
        self.id=id
    def set_name(self,name):
        self.name=name
    def set_price(self,price):
        self.price=price
        
    def get_id(self):
        return self.id
    def get_name(self):
        return self.name
    def get_price(self):
        return self.price
    def __str__(self):
        return "Product[id="+str(self.id) +"Name= "+self.name+"price="+str(self.price)+" ]"
    @staticmethod
    def search_productless_than_price(product_list,price):
        obj=filter(lambda p:p.get_price()<price,product_list)
        print(obj)
        return list(obj)

    def count_productless_than_price(product_list,price):
        obj=Product.search_productless_than_price(product_list,price)
        count=0
        for i in obj:
            count +=1
        return count

    def display_product_list(product_list):
        for product in product_list:
            print(product)

    def sum_of_productprice(product_list):
        prices=list(map(lambda p:p.get_price(),product_list))
        total_price=func.reduce(lambda x,y:x+y,prices)
        return total_price

    def summer_stats_of_product(product_list):
        prices=list(map(lambda p:p.get_price(),product_list))
        print("Total products:",len(product_list))

    def max_price_product_details(product_list):
        prices=list(map(lambda p:p.get_price(),product_list))
        return max(prices)

    def min_price_product_details(product_list):
        prices=list(map(lambda p:p.get_price(),product_list))
        return min(prices)

    def product_name_withPrice(product_list):
        return list(map(lambda p:print(f"Name:{p.get_name()},price:{p.get_price()}"),product_list))
    def show_list_of_product_name(product_list):
        return list(map(lambda p:print(p.get_name()),product_list))
product_list=[ Product(1, "Sony mobile", 25000),Product(2, "Lenovo mobile", 15000),Product(3, "Nokia mobile", 10000),Product(4, "Samsung mobile", 40000),Product(5, "Real Me", 50000)]
my_list=Product.search_productless_than_price(product_list,20000)
for i in my_list:
    print(i)

count_product=Product.count_productless_than_price(product_list,20000)
print(count_product)
print(Product.sum_of_productprice(product_list))
print(Product.sum_of_productprice(product_list))

Product.summer_stats_of_product(product_list)
print("Maximum prices's product in the list: ",Product.max_price_product_details(product_list))
print("Minimum prices's product in the list: ",Product.min_price_product_details(product_list))
Product.product_name_withPrice(product_list)       
Product.show_list_of_product_name(product_list)    
        
