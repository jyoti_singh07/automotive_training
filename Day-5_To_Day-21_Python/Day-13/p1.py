def fib_series():
    x,y=0,1
    while True:
        yield x
        x,y=y, x+y
fibonacci_gen=fib_series()

for i in range(8):
    print(next(fibonacci_gen))

