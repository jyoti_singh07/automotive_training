class InvalidAgeException(Exception):
    pass
class InvalidJobProfileException(Exception):
    pass
class InvalidNameException(Exception):
    pass
class Applicant:
    def __init__(self,name,jobprofile,age):
        self.__name=name
        self.__jobprofile=jobprofile
        self.__age=age
    def getName(self):
        return self.__name
    def setName(self,name):
        self.__name=name
    def getJobProfile(self):
        return self.__jobprofile
    def setJobProfile(self):
         self.__jobprofile=jobprofile
    def getAge(self):
        return self.__age
    def setAge(self,age):
         self.__age=age

class Validator:
    @staticmethod
    def validateName(name):
        if name.strip()!="" or name.strip() !=None:
            return True
        else:
            return False
    @staticmethod
    def validateJobProfile(jobprofile):
        if jobprofile =="Associate" or jobprofile=="clerk" or jobprofile=="executive" or jobprofile=="officer":
            return True
        else:
            return False
    @staticmethod
    def validateAge(age):
        if age>=18 and age<=30:
            return True
        else:
            return False
    @staticmethod
    def validate(applicant):
            if not Validator.validateName(applicant.getName()):
                raise InvalidNameException("Invalid name")
            
            if not Validator.validateAge(applicant.getAge()):
                raise InvalidAgeException("Invalid Age")

            if not Validator.validateJobProfile(applicant.getJobProfile()):
                raise InvalidJobProfileException("Invalid job profile")
            print("Application submitted successfully..")
            
def main():
    applicant=Applicant("Jyoti","Associate",24)
    try:
        Validator.validate(applicant)
    except InvalidAgeException as e:
        print(e)
    except InvalidNameException as e:
        print(e)
    except InvalidJobProfileException as e:
        print(e)
if __name__=="__main__":main()
                    

