"""
Problem Statement
Implement a program to display the sum of two given numbers if the numbers are same.
If the numbers are not same, display the double of the sum.
"""
res=lambda x,y:x+y if x==y else 2*(x+y)
print(res(4,6))

