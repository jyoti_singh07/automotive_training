"""
Problem Statement
You have x no. of 5 rupee coins and y no. of 1 rupee coins. You want to purchase an item
for amount z. The shopkeeper wants you to provide exact change. You want to pay using minimum
number of coins. How many 5 rupee coins and 1 rupee coins will you use? If exact change is not
possible then display -1.
"""
res=lambda no_of_five_coin, no_of_one_coin, amount_to_be_made:[amount_to_be_made %5, amount_to_be_made//5] if amount_to_be_made<=(no_of_five_coin*5 +  no_of_one_coin*1)and amount_to_be_made%5 else -1
print(res(4,2,21))

