import csv

class Student:
    def _init_(self, student_id, name, department, average_score, grade):
        self.student_id = int(student_id)
        self.name = name
        self.department = department
        self.average_score = float(average_score)
        self.grade = grade
        self.attendance = 0

class FetchStudentDetails:
    def _init_(self):
        self.student_list = []
        self.headers = []

    def get_data(self, file_name):
        with open(file_name, mode='r') as file:
            csv_reader = csv.reader(file)
            self.headers = next(csv_reader)  # Skip the headers
            for row in csv_reader:
                student = Student(*row)
                self.student_list.append(student)

    def get_attendance(self, attendance_file_name):
        with open(attendance_file_name, mode='r') as file:
            csv_reader = csv.reader(file)
            next(csv_reader)  # Skip the headers
            for row in csv_reader:
                student_id, attended = row
                student_id = int(student_id)
                attended = attended == 'Y'
                for student in self.student_list:
                    if student.student_id == student_id and attended:
                        student.attendance += 1

    def get_super_student(self):
        def modified_score(student):
            score = student.average_score
            if student.department == 'computer science':
                score += 5
            if student.department == 'maths':
                score += 10
            if student.name.startswith('y'):
                score += 15
            if student.student_id % 2 == 0:
                score += 20
            if student.grade == 'A+':
                score += 25
            return score

        return max(self.student_list, key=modified_score)

# Example usage:
fetcher = FetchStudentDetails()
fetcher.get_data('students.csv')
fetcher.get_attendance('attendance.csv')
super_student = fetcher.get_super_student()
print(super_student.name, super_student.student_id)
