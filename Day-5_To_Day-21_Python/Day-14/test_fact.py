import pytest
import factorial
@pytest.mark.fact
def test_factorial():
    actual_res=factorial.factorial(6)
    expected_res=720
    assert actual_res==expected_res
