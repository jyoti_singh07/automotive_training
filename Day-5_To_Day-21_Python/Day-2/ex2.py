"""
Problem Statement
Given a list of integer values. Write a python program to check whether
it contains same number in adjacent position. Display the count of such
adjacent occurrences
Sample Input	          Expected Output
[1,1,5,100,-20,-20,6,0,0]	3
[10,20,30,40,30,20]	        0
[1,2,2,3,4,4,4,10]	        3
"""
def count_adj_no_occurence(numbers):
    count=0
    for i in range(len(numbers)-1):
        if numbers[i]==numbers[i+1]:
            count +=1
    return count
numbers=[1,1,5,100,-20,-20,6,0,0]
#numbers=[10,20,30,40,30,20]
#numbers=[1,2,2,3,4,4,4,10]
print(count_adj_no_occurence(numbers))
