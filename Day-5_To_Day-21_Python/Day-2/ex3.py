"""
Problem Statement
Write a python program to display all the common characters between two
strings. Return -1 if there are no matching characters.
Note: Ignore blank spaces if there are any. Perform case sensitive string
comparison wherever necessary.
    Sample Input	                     Expected output
"I like Python"
"Java is a very popular language"	      lieyon
"""


def common_char_in_two_str(str1,str2):
    str1=str1.replace(" ","")
    str2=str2.replace(" ","")
    set1=set(str1)
    set2=set(str2)
    common_char=set1 & set2
    if common_char:
        return ''.join(common_char)
    else:
        return -1
str1="I like Python"
str2="Java is a very popular language"
print(common_char_in_two_str("I like Python","Java is a very popular language"))

