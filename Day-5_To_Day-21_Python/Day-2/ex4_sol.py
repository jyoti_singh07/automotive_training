def encrypt_sentence(sentence):
    #start writing your code here
    sentence=sentence.split(" ")
    l=[]
    v=["a","e","i","o","u","A","E","I","O","U"]
    for index,value in enumerate(sentence):
       # print(value)
        if index%2==0:
            s=value[::-1]
            l.insert(index,s)
        else:
            c=[]
            for i in value:
                if i not in v:
                    c.append(i)
            for j in value:
                if j in v:
                    c.append(j)
            d="".join(c)
            l.insert(index,d)
    return l
sentence="the sun rises in the east"
print(encrypt_sentence(sentence))
