'''Write a Python  program to print Fibonacci series up to n terms
Input number of terms: 10
0, 1, 1, 2, 3, 5, 8, 13, 21, 34
'''


n = 10
num1 = 0
num2 = 1
next_number = num2 
count = 1
while count <= n:
	print(next_number, end=" ")
	count += 1
	num1, num2 = num2, next_number
	next_number = num1 + num2
print()
