def fibo_series(n):
    fib_series = []
    a, b = 0, 1
    for _ in range(n):
        fib_series.append(a)
        a, b = b, a + b
    return fib_series
n = 10
fib_series = fibo_series(n)
print(', '.join(map(str, fib_series)))
