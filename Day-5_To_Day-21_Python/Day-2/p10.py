#How to access Element from the List
colors=['Red','Green','White','Blue','Orange']
print(colors[3])
print(colors[-3])

#iteration over the list
my_list=[90,80,70,60,50]
for ele in my_list:
    print(ele)

my_list=[1,2,3,4,5,6]
for (index, item) in enumerate(my_list):
    print('The item in position {} is: {}'.format(index, item))
