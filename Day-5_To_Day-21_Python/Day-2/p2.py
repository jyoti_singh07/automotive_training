'''How to access Character from the string:'''
#1.By using index
s="Country"
print(s[3])
print(s[0])
print(s[-3])
i=0
for x in s:
    print("the character {} present at index={}".format(x,i))
    i +=1


#2.By Using Slice Operator:
name="India"
print(name[0:3:])
print(name[:4:2])
print(name[::-1])
print(name[4:0:-1])



