
"""Question:-4
The encryption of alphabets are to be done as follows:
A = 1
B = 2
C = 3
.
.
.
Z = 26
 
The potential of a word is found by adding the encrypted value of the alphabets.
Example: KITE
Potential = 11 + 9 + 20 + 5 = 45
Accept a sentence which is terminated by either “ . ” , “ ? ” or “ ! ”. Each word of sentence is separated by single space. Decode the words according to their potential and arrange them in ascending order.
Output the result in format given below:
Example 1
 
INPUT       :   THE SKY IS THE LIMIT.
 
POTENTIAL   :   THE     = 33
               SKY     = 55
               IS      = 28
               THE     = 33
               LIMIT   = 63
 
OUTPUT      :   IS THE THE SKY LIMIT

""" 
