"""
Write a Python  program to print Fibonacci series up to n terms
Input
Input number of terms: 10
Output
Fibonacci series: 
0, 1, 1, 2, 3, 5, 8, 13, 21, 34
 
"""
 
def fabonacci(terms):
    if terms<=0:
        return
    elif terms==1:
        return 0
    v1=0
    v2=1
    series=[v1,v2]
    for i in range(terms-2):
        next_v=v1+v2
        series.append(next_v)
        v1=v2
        v2=next_v
    return series
 
print("Fibonacci series:",fabonacci(0))

 
 
        
 
 
 
 
 
 
 
 
 
    

""" Write a program to accept a sentence which may be terminated by either ‘.’ ‘?’ or ‘!’ only. Any other character may be ignored.
The words may be separated by more than one blank space and are in UPPER CASE.
Perform the following tasks:
(a)  Accept the sentence and reduce all the extra blank space between two words to
a single blank space.
(b) Accept a word from the user which is part of the sentence along with its
position number and delete the word and display the sentence.
Test your program with the sample data and some random data:
Example 1
INPUT:      
A    MORNING WALK IS A IS BLESSING FOR   THE  WHOLE DAY.
WORD TO BE DELETED: IS
WORD POSITION IN THE SENTENCE: 6
OUTPUT:  
A MORNING WALK IS A BLESSING FOR THE WHOLE DAY.
Example 2
INPUT:        
AS YOU    SOW, SO   SO YOU REAP.
WORD TO BE DELETED: SO
WORD POSITION IN THE SENTENCE: 4
 
"""
def normal_sentence(sentence):
    sentence = " ".join(sentence.split())
    return sentence
 
 
def delete_word(sentence, word_to_delete, position):
    words=sentence.split()
    if position<1 or position>len(words):
        return "Invalid position"
    if words[position-1]==word_to_delete:
        words.pop(position-1)
    else:
        return "Word at the given position does not match the word to be deleted"
    return ' '.join(words)
 
 
sentence= "A    MORNING WALK IS A IS BLESSING FOR   THE  WHOLE DAY."
normal_sentence= normal_sentence(sentence)
word_to_delete= "IS"
position= 6
 
output= delete_word(normal_sentence, word_to_delete, position)
print(output)

"""

Write a program to accept a sentence which may be terminated by either ‘.’ or ‘?’ only.
The words are to be separated by a single blank space.
Print an error message if the input does not terminate with ‘.’ or ‘?’.
You can assume that no word in the sentence exceeds 15 characters, so that you get a proper formatted output.
Perform the following tasks:
(i) Convert the first letter of each word to uppercase.
(ii) Find the number of vowels and consonants in each word and display them
with proper headings along with the words.
Test your program with the following inputs.
Example 1
INPUT: Intelligence plus character is education.
OUTPUT:
Intelligence Plus Character Is Education
Word	Vowels	Consonants
Intelligence	5	7
Plus	1	3
Character	3	6
Is	1	1
Education	5	4
 
 
"""
def count_vowel_consonant(sentence):
 
    if not (sentence.endswith('.') or sentence.endswith('?') or sentence.endswith('!')):
        print("Sentence must end with either '.' or '?' or '!'")
        return
 
    sentence = sentence[:-1].title()
    print(sentence)
 
 
    print("Word\t\tVowels consonants")
    for word in sentence.split():
        vowel_count=0
        consonant_count=0
        for vowel in word.lower():
            if vowel == "a" or vowel == "e" or vowel == "i" or vowel == "o" or vowel == "u":
                vowel_count+=1
            else:
                consonant_count+=1
        print(word,"\t",vowel_count,"\t",consonant_count)
 
sentence = "Intelligence plus character is education."
 
count_vowel_consonant(sentence)
 
 
 
 
 
""" 
Write a program to accept a sentence which may be terminated by either’.’, ‘?’or’!’ only.
The words may be separated by more than one blank space and are in UPPER CASE.
Perform the following tasks:
(a) Find the number of words beginning and ending with a vowel.
(b) Place the words which begin and end with a vowel at the beginning, followed by the remaining words as they occur in the sentence.
Test your program with the sample data and some random data:
Example 1
INPUT: ANAMIKA AND SUSAN ARE NEVER GOING TO QUARREL ANYMORE.
OUTPUT: NUMBER OF WORDS BEGINNING AND ENDING WITH A VOWEL= 3
ANAMIKA ARE ANYMORE AND SUSAN NEVER GOING TO QUARREL
Example 2
INPUT: YOU MUST AIM TO BE A BETTER PERSON TOMORROW THAN YOU ARE TODAY.
OUTPUT: NUMBER OF WORDS BEGINNING AND ENDING WITH A VOWEL= 2
A ARE YOU MUST AIM TO BE BETTER PERSON TOMORROW THAN YOU TODAY
Example 3
INPUT: LOOK BEFORE YOU LEAP.
OUTPUT: NUMBER OF WORDS BEGINNING AND ENDING WITH A VOWEL= 0
LOOK BEFORE YOU LEAP
Example 4
INPUT: HOW ARE YOU@
OUTPUT: INVALID INPUT
 
"""
def count_vowel_beginning_end(sentence):
    if not (sentence.endswith(".") or sentence.endswith("?") or sentence.endswith("!")):
        print("INVALID INPUT")
        return
    sentence=sentence[:-1].upper().split()
    vowels={"A", "E", "I", "O", "U"}
    vowel_words=[]
    other_words=[]
 
    for word in sentence:
        word = word.strip()
        if word[0] in vowels and word[-1] in vowels:
            vowel_words.append(word)
        else:
            other_words.append(word)
    reordered_sentence=vowel_words+other_words
    print(f"NUMBER OF WORDS BEGINNING AND ENDING WITH A VOWEL= {len(vowel_words)}")
    print(" ".join(reordered_sentence))
 
count_vowel_beginning_end("ANAMIKA AND SUSAN ARE NEVER GOING TO QUARREL ANYMORE.")
count_vowel_beginning_end("YOU MUST AIM TO BE A BETTER PERSON TOMORROW THAN YOU ARE TODAY.")
count_vowel_beginning_end("LOOK BEFORE YOU LEAP.")
count_vowel_beginning_end("HOW ARE YOU@")

""" 
The encryption of alphabets are to be done as follows:
A = 1
B = 2
C = 3
.
.
.
Z = 26
The potential of a word is found by adding the encrypted value of the alphabets.
Example: KITE
Potential = 11 + 9 + 20 + 5 = 45
Accept a sentence which is terminated by either “ . ” , “ ? ” or “ ! ”. Each word of sentence is separated by single space. Decode the words according to their potential and arrange them in ascending order.
Output the result in format given below:
Example 1
INPUT       :   THE SKY IS THE LIMIT.
POTENTIAL   :   THE     = 33
               SKY     = 55
               IS      = 28
               THE     = 33
               LIMIT   = 63
OUTPUT      :   IS THE THE SKY LIMIT
 
 
"""
def calculate_potential(word):
    total = 0
    uppercase_word = word.upper()
    for char in uppercase_word:
        value = ord(char) - ord('A') + 1
        total += value
    return total
 
def process_sentence(sentence):
    words = sentence[:-1].split()
    potentials = [(word, calculate_potential(word)) for word in words]
    sorted_words = sorted(potentials, key=lambda x: x[1])
    potential_output = "\n".join(f"{word} = {potential}" for word, potential in potentials)
    sorted_words_output = " ".join(word for word, _ in sorted_words)
    return potential_output, sorted_words_output
 
sentence = "THE SKY IS THE LIMIT."
potential_output, sorted_words_output = process_sentence(sentence)
print(potential_output)
print(sorted_words_output)
