sample_dict = {'a':1,'b':2}
sample_dict.update({'b':5, 'c':10 })
print(sample_dict.get('b'), sample_dict.get('c'))

my_library ={
    103 : "Alice in Wonderland",
    104 : "The Turning Point",
    113 : "Wings on Fire",
    134 : "Harry Potter"
}
print(my_library[104])
