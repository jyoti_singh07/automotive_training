"""
Write a Python program to get the maximum
and minimum values of a dictionary.
"""
my_dict = {'a': 10, 'b': 20, 'c': 5, 'd': 40}
max_value=max(my_dict.values())
min_value=min(my_dict.values())
print(max_value,min_value)
