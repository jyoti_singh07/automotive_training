class Mobile:
    def __init__(self,brand,price):
        print("It is a parameterized constructor")
        print("Id of self in constructor",id(self))
        self.brand=brand
        self.price=price
mob1=Mobile("Vivo",12000)
print("id of mob1",id(mob1))
print("Mobil1 has brand: ",mob1.brand,"and price is=",mob1.price)
mob2=Mobile("redmi",14000)
print("Mobil2 has brand:",mob2.brand,"and price is=",mob2.price)
print("id of mobile2 is",id(mob2))
