class C1:
    def __init__(self):
        self.a=10
        print('C1-init')
class C2(C1):
    def __init__(self):
        super().__init__()
        print('C2-init')
obj=C2()
print(obj.a)
