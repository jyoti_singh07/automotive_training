class Phone:
    def __init__(self, price, brand, camera):
        print ("Inside phone constructor")
        self.__price = price
        self.brand = brand
        self.camera = camera
    def buy(self):
        print ("Buying a phone")
    def return_phone(self):
        print ("Returning a phone")
p=Phone(1000,"mi","12px")
p=Phone(1000,"mi","12px").buy()
p=Phone(1000,"mi","12px").return_phone()
class FeaturePhone(Phone):
    pass

class SmartPhone(Phone):
    def __init__(self, os, ram):
        self.os = os
        self.ram = ram
        print ("Inside SmartPhone constructor")
    def buy(self):
        print ("Buying a SmartPhone")
s=SmartPhone("Android", 2)
print(s.os)
print(s.ram)
s=SmartPhone("Android", 2).buy()
