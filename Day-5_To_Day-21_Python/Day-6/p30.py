class Student:
 college_Name="ITM"
 
# instantiate the Student class 
s1=Student()
#acessing static variable using ClassName Outside the class
print(Student.college_Name)
#acessing static variable using object reference Outside the class
print(s1.college_Name)
#modifying the static variable outside class
Student.college_Name="IIT"
print("After modifying static variable outside class")
#acessing static variable using ClassName Outside the class
print(Student.college_Name)
#acessing static variable using object reference Outside the class
print(s1.college_Name
