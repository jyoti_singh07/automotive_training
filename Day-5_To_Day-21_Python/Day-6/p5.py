class Person:
    def __init__(self,name,age,sex,weight,height):
        self.name=name
        self.age=age
        self.sex=sex
        self.weight=weight
        self.height=height
    def eating(self,msg):
        print(msg)
    def displayPersonInfo(self):
        print("Name: ",self.name)
        print("Age: ",self.age)
        print("Sex: ",self.sex)
        print("Weight: ",self.weight)
        print("Height: ",self.height)
mark=Person("Manav",45,"Male",30,5.2)
mark.displayPersonInfo()
mark.eating("Can eat only Non-veg Food")
print("===============================")
jiya=Person("Jiya",25,"Female",45,5.1)
jiya.displayPersonInfo()
jiya.eating("Can eat only Veg Food")
