"""
Problem Statement
"Mysore cabs" wants to automate their booking service.
Write a python program to implement the class diagram given below.
                               
Class description
CabRepository:
Initialize static lists, cab_type_list, charge_per_km and no_of_cars using the sample data given in the table
There is one to one correspondence between these static lists

Cab type list        Charge per km              Number of cars
	
Hatch Back            9                            2
	
Sedan                12                            5
	 
SUV                  5                             10

CabService:
check_availability(): Check whether the requested cab type is available or not for booking by checking in CabRepository.cab_type_list. If available
return index position of the cab type in CabRepository.cab_type_list. Else return -1
get_cab_charge(index): Find and return the charge per km for the car at the given index position from CabRepository.charge_per_km list
calculate_waiting_charge( waiting_time_mins): Calculate and return waiting charge based on the given waiting_time_mins
For first 30 minutes there is no waiting charge
After 30 minutes, 5 rupees should be charged for every extra minute
booking(waiting_time_mins): Calculate and return the final amount to be paid by the customer including the waiting charge for given waiting_time_mins.
Also update the number of available cars and generate the service id for each booking starting from 1001. Return -1 if the car is not available.
Perform case sensitive string comparison.
Create objects of CabService class. Invoke booking() on CabService class by passing waiting time in mins and display the details.
"""


