from abc import ABC, abstractmethod
class Test1(ABC):
    def m1(self):
        pass
    def m2(self):
        pass
    def m3(self):
        pass
class Test2(Test1):
    def m1(self):
        print("m1-method defined by test2 class")
    def m2(self):
        print("m2-method defined by test2 class")
    def m3(self):
        print("m3-method defined by test3 class")
t2=Test2()
t2.m1()
t2.m2()
t2.m3()
