from abc import ABC, abstractmethod
class Polygon(ABC):
    @abstractmethod
    def noOfSides(self):
        pass
class Triangle(Polygon):
    def noOfSides(self):
        print("Triangle have 3 sides")
class Pentagon(Polygon):
    def noOfSides(self):
        print("Pentagon have 5 sides")
class Hexagon(Polygon):
    def noOfSides(self):
        print("Hexagon have 6 sides")
class Quadrilateral(Polygon):
    def noOfSides(self):
        print("Quadrilateral have 4 sides")
T=Triangle()
T.noOfSides()
Q=Quadrilateral()
Q.noOfSides()
P=Pentagon()
P.noOfSides()
H=Hexagon()
H.noOfSides()
    
