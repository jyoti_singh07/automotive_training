from abc import ABC, abstractmethod
class Shape(ABC):
    @abstractmethod
    def area(self):
        pass
    @abstractmethod
    def perimeter(self):
        pass
class Rectangle(Shape):
    def __init__(self,l,b):
        self.l=1
        self.b=b
    def area(self):
        return(self.l*self.b)
    def perimeter(self):
        return(2*(self.l+self.b))
class Square(Shape):
    def __init__(self,side):
        self.side=side
    def area(self):
        return (self.side*self.side)
    def perimeter(self):
        return (4*self.side)
class Circle(Shape):
    def __init__(self,radius):
        self.radius=radius
    def area(self):
        return (3.14*self.radius*self.radius)
    def perimeter(self):
        return (2*3.14*self.radius)
r1=Rectangle(10,20)
print("Area of rectangle= ",r1.area())
print("Perimeter of rectangle= ",r1.perimeter())
s1=Square(10)
print("Area of square= ",s1.area())
print("Perimeter of square= ",s1.perimeter())
c1=Circle(7)
print("Area of circle= ",c1.area())
print("Perimeter of circle= ",c1.perimeter())
