from abc import ABC, abstractmethod
class Plan:
    def __init__(self):
        self.rate=0.0
    @abstractmethod
    def setRate(self):
        pass
    def calculateBill(self,units):
        print(units*self.rate)
class DomesticPlan(Plan):
    def setRate(self):
        self.rate=3.6
class CommercialPlan(Plan):
    def setRate(self):
        self.rate=4.2
class InstitutionalPlan(Plan):
    def setRate(self):
        self.rate=5.3
class GetPlanFactory:
    def 
