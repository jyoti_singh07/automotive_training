class India():
    def capital(self):
        print("New Delhi")
    def language(self):
        print("Hindi and english")
class USA():
    def capital(self):
        print("Washington, D.C.")
    def language(self):
        print("English")
class Europe():
    def capital(self):
        print("Brussels")
    def language(self):
        print("French")
obj_ind=India()
obj_usa=USA()
obj_eur=Europe()
for country in (obj_ind,obj_usa,obj_eur):
    country.capital()
    country.language()
