def find_average(m_list):
    total=0
    try:
        for i in range(0,len(m_list)+1):
            total=total+m_list[i]
        return total/len(m_list)
    except ValueError:
        print("Value error occurred")
    except TypeError:
        print("Type error occurred")
    except ZeroDivisionError:
        print("Zero division error occurred")
    except IndexError:
        print("index errror occurred")
    except:
        print("Some error occurred")     
try:
    m_list=[]
    #m_list=[1,2,3,4]
    #m_list=[mark1,2,3,4]
    find_average(m_list)
except ValueError:
    print("Value error occurred")
except NameError:
    print("Name error occurred")
except:
    print("Some error occurred")
        
        
