"""
5. Write a python program to generate a function to double a specified number.

The output is:

Specified number: 27.2

Expected Answer:
The double number of 27.2 = 54.4
"""
double=lambda n:"The double of the number {}={}" .format(n,2*n)
print(double(27.2))
