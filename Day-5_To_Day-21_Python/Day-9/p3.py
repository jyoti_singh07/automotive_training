# Python program to overload
# a comparison operators 
class A:
    def __init__(self,a):
        self.a=a
    def __gt__(self,other):
        if(self.a>other.a):
            return True
        else:
            return False
obj1=A(2)
obj2=A(5)
if(obj1>obj2):
    print("Obj1 is greater than obj2")
else:
    print("obj1 is lesser than obj2")
