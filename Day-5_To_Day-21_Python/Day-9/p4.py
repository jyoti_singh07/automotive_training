# Python program to overload equality
# and less than operators

class A:
    def __init__(self,a):
        self.a=a
    def __lt__(self,other):
        if(self.a < other.a):
            return "obj1 is lesser than obj2"
        else:
            return "obj2 is lesser than obj1"
    def __eq__(self,other):
        if(self.a==other.a):
            return "Both are equal"
        else:
            return "Not Equal"
obj1=A(3)
obj2=A(7)
print(obj1<obj2)
obj3=A(9)
obj4=A(2)
print(obj3==obj4)
