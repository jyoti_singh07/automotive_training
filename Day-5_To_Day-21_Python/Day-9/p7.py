#Single inheritance
class Phone:
    def __init__(self,price,brand,camera):
        print("Inside phone constructor")
        self.price=price
        self.brand=brand
        self.camera=camera
    def buy(self):
        print("Buying a phone")
    def return_phone(self):
        print("Returning a phone")
class Smartphone(Phone):
    pass
Smartphone(9000,"oppo","12px").buy()
Smartphone(9000,"oppo","12px").return_phone()
