"""
Write a python function to add 'ing' at the end of a given string and return the new
string. 
If the given string already ends with 'ing' then add 'ly'.
If the length of the given string is less than 3, leave it unchanged.

Sample Input	Expected Output
sleep	         sleeping
amazing	         amazingly
is	           is
 
"""

def add_suffix(word):
    new_word=""
    if len(word)<3:
       new_word=word 
    else:
        if word.endswith("ing"):
            new_word=word+"".join("ly")
        else:
            new_word=word+"".join("ing")
    return  new_word
word="sleep"
print(add_suffix(word))
word="amazing"
print(add_suffix(word))
word="is"
print(add_suffix(word))
