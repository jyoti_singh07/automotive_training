"""
Problem Statement
Write a Python function which accepts a string and returns a string made
of the first 2 and the last 2 characters of the given string.
If the string length is less than 2, return -1.
Note: If the string length is equal to 2, consider the 2 characters to
be the first as well as the last two characters.
Sample Input	Expected Output
w3resource	 w3ce
w3	         w3w3
A	         -1
"""
def return_string(str1):
    if len(str1)<2:
        return -1
    elif len(str1)==2:
        str2=str1+"".join(str1)
        return str2
    else:
        str3=str1[0:2:]+"".join(str1[-2::])
        return str3
s1="w3resource"
print(return_string(s1))
s2="w3"
print(return_string(s2))
s3="A"
print(return_string(s3))
