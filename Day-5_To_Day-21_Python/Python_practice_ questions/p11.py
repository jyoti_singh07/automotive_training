"""
Problem Statement

Write a python function which accepts a sentence and returns a
list in which first
value is the count of upper case letters and second value is the count
of lower case letters
in the sentence. Ignore spaces, numbers and other special characters if any.
Sample Input	   Expected Output
Hello world!	       [1,9]
Welcome to Mysoren      [2,14]
"""

def count_upper_and_lower_case_letter(sentence):
    a_z="abcdefghijklmnopqrstuvwxyz"
    A_Z=a_z.upper()
    count_upper=0
    count_lower=0
    spcl_char=0
    for char in sentence:
        if char in A_Z:
            count_upper +=1
        elif char in a_z:
            count_lower +=1
        else:
            spcl_char +=1
    return[count_upper,count_lower]

s1="Hello world!"
print(count_upper_and_lower_case_letter(s1))
s2="Welcome to Mysoren"
print(count_upper_and_lower_case_letter(s2))
    
    
 

