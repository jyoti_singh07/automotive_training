"""
Problem Statement
Write a python function which accepts a list of numbers and returns
true if the list contains a 2 next to a 2. Otherwise it should return false.
Sample Input	         Expected Output
[ 1,2,1,2,3,4,5,2,2]	     True
[3,2,5,1,2,1,2]      	     False
"""

def two_next_to_two(l):
    res=False
    for i in range(len(l)-1):
        if l[i]==2:
            if l[i]==2 and l[i+1] !=2:
                continue
            elif l[i]==2 and l[i+1]==2:
                res= True
    return res
print(two_next_to_two([ 1,2,1,2,3,4,5,2,2]))
print(two_next_to_two([3,2,5,1,2,1,2]))
print(two_next_to_two([3,2,5,1,2,1,2,2,2]))

