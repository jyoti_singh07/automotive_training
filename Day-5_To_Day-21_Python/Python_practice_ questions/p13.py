"""
Write a python function to find out whether a number is divisible
by
the sum of its digits. If so return True,else return False.
Sample Input	Expected Output
42	            True
66	           False
"""

def divisibility_check_sum_of_digit(number):
    sum_of_digit=0
    previous_number=number
    while number>0:
        last_digit=number%10
        sum_of_digit +=last_digit
        number //=10
    if previous_number% sum_of_digit==0:
        return True
    else:
        return False
print(divisibility_check_sum_of_digit(42))
print(divisibility_check_sum_of_digit(66))
 
