"""
Write a python function that accepts a list of words and returns the list
of integers representing the length of the corresponding words.
Sample Input	Expected Output
[cat, Come]	[3,4]
"""
def find_length_of_word(words):
    return [len(word)for word in words]
my_word=["cat", "Come"]
print(find_length_of_word(my_word))
 
