"""
Given 2 positive integers, write a python function to return True if one
of them is 10 or if their sum is 10, else return False.
Sample Input	Expected Output
10,9	           True
2,8	           True
2,9	           False
"""
def check_sum_or_digit_isTen(n1,n2):
    if n1==10 or n2==10 or (n1+n2)==10:
        return True
    else:
        return False
print(check_sum_or_digit_isTen(10,9))
print(check_sum_or_digit_isTen(2,8))
print(check_sum_or_digit_isTen(2,9))


