"""
Given a list of numbers, write a python function which returns true if
one of the first 4 elements in the list is 9. Otherwise it should
return false.
The length of the list can be less than 4 also.
Sample Input	Expected Output
[1, 2, 9, 3, 4]	True
[1, 2, 9]	True
[1, 2,3,4]	False
"""
def find_elements_in_list(l):
    if l[0]==9 or l[1]==9 or l[2]==9 or l[3]==9:
        return True
    else:
        return False
my_list1=[1, 2, 9, 3, 4]
print(find_elements_in_list(my_list1))
my_list2=[1, 2, 9]
print(find_elements_in_list(my_list2))
my_list3=[1, 2,3,4]
print(find_elements_in_list(my_list3))
