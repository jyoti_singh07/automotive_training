"""
Write a python function which accepts a sentence and finds the number of
letters and digits in the sentence.
It should return a list in which the first value should be letter count
and second value should be digit count.
Ignore the spaces or any other special character in the sentence.

Sample Input	Expected Output
Infosys 123	    [7,3]
ABCEFG	            [6,0]

"""

def find_no_of_letter_digit(s):
    s=s.split()
    s_without_space=""
    for word in s:
        s_without_space=s_without_space+"".join(word)
    count_digit=0
    count_char=0
    for ele in s_without_space:
        if ele.isdigit():
            count_digit+=1
        else:
            count_char +=1
    return [ count_char, count_digit ]
            
s1="Infosys 123"
print(find_no_of_letter_digit(s1))
s2="ABCEFG"
print(find_no_of_letter_digit(s2))
