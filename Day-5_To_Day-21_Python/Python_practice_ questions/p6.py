"""
Problem Statement
Write a python function which accepts a list of numbers
and returns true,
if 1, 2, 3 appears in sequence in the list.
Otherwise, it should return false.
Sample Input	Expected Output
[1, 1, 2, 3, 1]	     True
[1, 1, 2, 4, 3]	     False
"""
def check_sequence_of_number(my_list):
    for i in range(len(my_list)):
        if my_list[i]==1 and my_list[i+1] !=1:
            if my_list[i]==1 and my_list[i+1]==2 and my_list[i+2]==3:
                return True
            else:
                return False
my_list1=[1, 1, 2, 3, 1]
print(check_sequence_of_number(my_list1))
my_list2=[1, 1, 2, 4, 3]
print(check_sequence_of_number(my_list2))

