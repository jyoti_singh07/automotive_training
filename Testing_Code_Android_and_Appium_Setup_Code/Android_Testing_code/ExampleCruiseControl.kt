package com.example.androidtest
import org.junit.Test
import org.junit.Assert.*
class ExampleCruiseControl {
     val safetyFeatures=SafetyFeatures()
    @Test
    fun testSpeedLimiter(){
        assertEquals(100,safetyFeatures.setSpeed(100))
    }
    @Test
    fun testObstacleDetection(){
        assertEquals(40,safetyFeatures.detectObstacle(45))
    }
    @Test
    fun testDriverAlert(){
        assertEquals(30,safetyFeatures.alertDriver(40))
    }

}