package com.example.androidtest
import org.junit.Test
class ExampleMusic {
    @Test
    fun play(){
        val exampleMusic=ExampleMusic()
        exampleMusic.play()//Add assertion to verify playback started
    }
    @Test
    fun pause(){
        val exampleMusic=ExampleMusic()
        exampleMusic.pause()//Add assertion to verify playback paused
    }
    @Test
    fun stop(){
        val exampleMusic=ExampleMusic()
        exampleMusic.stop()//Add assertion to verify playback stopped
    }
}