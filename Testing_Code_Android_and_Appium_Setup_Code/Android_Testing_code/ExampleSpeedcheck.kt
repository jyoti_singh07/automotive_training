package com.example.androidtest
import org.junit.Test
import org.junit.Assert.*
//This test class will demonstrate the individual unit test cases of a Automotive product
class ExampleSpeedcheck {
    //This test case is written for speed limit check
    @Test
    fun check_limitSpeed()
    {
        assertEquals(40, 40)
    }
    @Test
    fun check_SpeednotInLimit()
    {
        assertEquals(40, 60)
    }

}