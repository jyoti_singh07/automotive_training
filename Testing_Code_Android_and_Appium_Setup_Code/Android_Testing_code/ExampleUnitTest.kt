package com.example.androidtest

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
// These are  the test cases for unit testing of simple calculator function add,substract,multiplication,division
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
    @Test
    fun addition_isInCorrect()
    {
        assertEquals(4, 2 + 3)
    }
    @Test
    fun substraction_isCorrect()
    {
        assertEquals(0, 2 - 2)
    }

    @Test
    fun substraction_isInCorrect()
    {
        assertEquals(0, 5 - 3)
    }
    @Test
    fun multiplication_isCorrect()
    {
        assertEquals(6, 2 * 3)
    }
    @Test
    fun multiplication_isInCorrect()
    {
        assertEquals(6, 2 * 5)
    }
    @Test
    fun division_isCorrect()
    {
        assertEquals(2, 4 / 2)
    }
    @Test
    fun division_isInCorrect()
    {
        assertEquals(2, 6 / 2)
    }
}