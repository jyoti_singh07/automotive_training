package com.example.androidtest
import org.junit.Test
import org.junit.Assert.*
//these are the unit test cases for simple interest(SI) and principle amount(PA)
class ExampleUnitTest2 {
        @Test
        fun calculate_SI()
        {
            assertEquals(80, (1000*2*4)/100)
        }
        @Test
        fun calculate_PA()
        {
            assertEquals(1250, (100*100)/(2*4))
        }

}